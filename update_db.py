#!/usr/bin/env python3
"""
The purpose of this script is to add paragraph indicators to the relevent
verses in the sqlite KJV database.
"""

import sqlite3


def get_text_lines(filename):
    r = None
    with open(filename) as fobj:
        r = fobj.readlines()
    return r


def get_db_conn(filename):
    return sqlite3.connect(filename)


def parse_line_info(line):
    book, chap_verse = line.split(" ")[:2]
    chapter, verse = chap_verse.split(":")
    print("B: %s, C: %s, V: %s" % (book, chapter, verse))
    return book, chapter, verse


def update_verse_from_line(line, cur):
    try:
        book, chapter, verse = parse_line_info(line)
    except Exception as ex:
        print("Failing line: '%s'" % line)
        return
    c.execute(
    """UPDATE Bible SET VText='¶ ' || VText
    WHERE BookAbr=?
    AND Chapter=?
    AND Verse=?;
    """, (book, chapter, verse))


if __name__ == "__main__":
    conn = get_db_conn("KJV-PCE.db")
    c = conn.cursor()
    for r in get_text_lines("WHARTON_PCE.txt"):
        if "¶" in r:
            update_verse_from_line(r, conn)
    conn.commit()
