# Motivation

The King James Bible was written in modern English, not to be confused with the _contemporary_ english we speak now.  This is an attempt to update the KJV Bible to contemporary English, with as few changes as necessary.  Being a speaker of contemporary English, I perform the translations found in this version on the fly while reading the original King James.  Converting the KJV to contemporary English reduces the cognitive load while reading.

## Source material

The source material was downloaded from [bibleprotector.com](http://bibleprotector.com).  I used the sqlite database version from that site.  The sqlite version from there is missing the paragraph markers found in most copies, so I also used the WHARTON\_PCE.txt version to parse the paragraph markers and add them to the sqlite database.  The script update\_db.py found in this repository performs that update.

## Methodology

All changes to the original KJV to create this contemporary KJV are found in `bible_transforms.txt`.  Single words are substituted for their contemporary replacement.  No changes to grammar or rewording of sentences occurs.  This is a spelling update.  The substitions fall mainly into 3 categories: pronouns (changing ye, thee, thine to you/your), verbs ending in est (criest becomes cry), and verbs ending in eth (crieth becomes cries).  In addition some archaic spellings were updated to modern spellings (shew becomes show).

To find the verbs which should be changed, a list of all words in the Bible was generated, then filtered for words ending in est or eth.  These words were then inspected manually and referenced with multiple dictionaries to determine the correct verb and tense for substitution.  Additional substitutions were/are added when I come across them in my daily Bible reading.

## Output

This is still a work in progess.  For now I use the `bible.sh` script in the ckjv folder to generate the new version on the fly.  The script takes a partial of the name of the book as the first argument, and a comma separated list of chapters as the second argument, e.g. `./bible.sh Rom 4,5`, which will print Romans chapters 4 and 5 to the terminal.

Eventually the goal is to create a digital book in epub format.
