from sqlite3 import connect
from ebooklib.epub import EpubBook
from ebooklib.epub import EpubHtml
from ebooklib.epub import Section
from ebooklib.epub import EpubNcx
from ebooklib.epub import EpubNav
from ebooklib.epub import write_epub
from bs4 import BeautifulSoup as BS


def set_metadata(book: EpubBook):
    book.set_identifier("lNpkMq6AIRq7HVZJo8ajDilvYMcPCeKg")
    book.set_title("Contemporary King James Bible")
    book.set_language("en")
    book.add_author("God")


def make_html(bookname, chapter, verses: [tuple]) -> [str]:
    c = BS("<div></div>")
    div = c.div
    heading = c.new_tag("h2")
    heading.append("%s %d" % (bookname, chapter))
    div.append(heading)
    for v in verses:
        p = c.new_tag("p")
        p.append("%s %s" % (v[4], v[-1]))
        div.append(p)
    ehtml = EpubHtml(
        uid="%s_%d" % (bookname, chapter),
        file_name="%s_%d.html" % (bookname, chapter),
        content=div.prettify(),
        title="%s %d" % (bookname, chapter),
    )
    #ehtml.add_link(href="styles.css", rel="stylesheet", type="text/css")
    return ehtml


def get_chapters_for_book(conn, book_no):
    sql = """
    SELECT BookName, max(cast(chapter as int)) FROM bible
    WHERE BookID=?;
    """
    res = conn.execute(sql, (book_no,))
    return res.fetchone()


def get_verses_for_chapter(conn, book_no, chapter):
    sql = """
    SELECT * FROM bible
    WHERE BookID=?
    AND Chapter=?
    ORDER BY CAST(Verse as int);
    """
    res = conn.execute(sql, (book_no, chapter))
    return res.fetchall()


if __name__ == "__main__":
    conn = connect("../KJV-PCE.db")
    book = EpubBook()

    toc = []
    spine = ["id"]
    for book_no in range(1, 67):
        bookname, chapters = get_chapters_for_book(conn, book_no)
        print("Processing %s, %d chapters" % (bookname, chapters))
        chapter_htmls = []
        for chapter in range(1, chapters + 1):
            verses = get_verses_for_chapter(conn, book_no, chapter)
            ehtml = make_html(bookname, chapter, verses)
            chapter_htmls.append(ehtml)
            spine.append(ehtml)
            book.add_item(ehtml)
        toc.append((Section(bookname), chapter_htmls))

    book.toc = toc
    book.spine = spine
    book.add_item(EpubNcx())
    book.add_item(EpubNav())
    write_epub("ckjv-bible.epub", book)
