extern crate regex;

#[macro_use]
extern crate horrorshow;
use horrorshow::prelude::*;

use regex::Regex;

use rusqlite::types::ToSql;
use rusqlite::{Connection, NO_PARAMS};

use std::fs::File;
use std::io::{self, BufRead, BufReader};

struct BVerse {
    book_no: i64,
    book_abr: String,
    book_name: String,
    chapter: i64,
    verse: i64,
    verse_text: String,
}

struct BTransform {
    book_name: Option<String>,
    chapter: Option<i64>,
    verse: Option<i64>,
    original: String,
    replacement: String,
}

fn apply_substitutions(s: String, subs: &Vec<BTransform>) -> String {
    let mut t: String = s.clone();

    for sub in subs.iter() {
        if t.contains(sub.original.as_str()) {
            let re = Regex::new(&format!(
                "^{0}[ .,:;?]|[ \\[]{0}[ .,:;!?\\]]|[ .,:;?!]{0}$",
                sub.original
            ))
            .unwrap();
            for cap in re.captures_iter(t.clone().as_str()) {
                t = t.replace(
                    &cap[0],
                    &cap[0].replace(sub.original.as_str(), sub.replacement.as_str()),
                );
            }
            //t = n.into_owned();
        }
    }

    t = apply_mine_substitutions(t);
    t = apply_thine_substitutions(t);

    t
}

fn apply_mine_substitutions(s: String) -> String {
    let mut t: String = s.clone();

    if t.contains("mine") {
        // Because we are potentially replacing a 4 letter word with a 2 letter word,
        // we need to rerun the regex after replacement so that indexes are aligned
        // properly in the case of a verse with more than 1 instance of mine, whose
        // replacements are not necessarily the same (e.g. Psalm 108:8)
        let mut exists = true;
        let re = Regex::new("^mine [AEIOUHaeiouh]|\\Wmine [AEIOUHaeiouh]").unwrap();
        while exists {
            let caps = re.find(t.as_str());
            t = match caps {
                Some(cap) => {
                    exists = true;
                    let start = cap.start();
                    let end = cap.end();
                    let replacement = cap.as_str().replace("mine", "my");
                    t.replace_range(start..end, replacement.as_str());
                    t
                }
                None => {
                    exists = false;
                    t
                }
            }
        }
    }
    if t.contains("Mine") {
        let re = Regex::new("^Mine [AEIOUHaeiouh]|Mine [AEIOUHaeiouh]").unwrap();
        let caps = re.find(t.as_str());
        t = match caps {
            Some(_) => t.replace("Mine", "My"),
            None => t,
        }
    }
    t
}

fn apply_thine_substitutions(s: String) -> String {
    let mut t: String = s.clone();

    if t.contains("thine") {
        // This handling is wrong.  Will substitute 'yours' in filthiness, e.g.
        let mut exists = true;
        let re = Regex::new("^thine [AEIOUHaeiouh]| thine [AEIOUHaeiouh]").unwrap();
        while exists {
            let caps = re.find(t.as_str());
            t = match caps {
                Some(cap) => {
                    exists = true;
                    let start = cap.start();
                    let end = cap.end();
                    let replacement = cap.as_str().replace("thine", "your");
                    t.replace_range(start..end, replacement.as_str());
                    t
                }
                None => {
                    exists = false;
                    t
                }
            }
        }
        exists = true;
        let re = Regex::new("^thine [^AEIOUHaeiouh]| thine [^AEIOUHaeiouh]").unwrap();
        while exists {
            let caps = re.find(t.as_str());
            t = match caps {
                Some(cap) => {
                    exists = true;
                    let start = cap.start();
                    let end = cap.end();
                    let replacement = cap.as_str().replace("thine", "yours");
                    t.replace_range(start..end, replacement.as_str());
                    t
                }
                None => {
                    exists = false;
                    t
                }
            }
        }
    }

    if t.contains("Thine") {
        let re = Regex::new("^Thine [AEIOUHaeiouh]| Thine [AEIOUHaeiouh]").unwrap();
        let caps = re.find(t.as_str());
        t = match caps {
            Some(_) => t.replace("Thine", "Your"),
            None => t.replace("Thine", "Yours"),
        }
    }
    t
}

fn apply_substitutions_verse(ts: &Vec<BTransform>, b: &BVerse) -> BVerse {
    let s = apply_substitutions(b.verse_text.clone(), &ts);
    let b = BVerse {
        book_no: b.book_no,
        book_abr: b.book_abr.clone(),
        book_name: b.book_name.clone(),
        chapter: b.chapter,
        verse: b.verse,
        verse_text: s,
    };
    b
}

fn parse_transforms(filename: &str) -> Vec<BTransform> {
    let f = File::open(filename).unwrap();
    let file = BufReader::new(&f);
    let mut subs: Vec<BTransform> = Vec::new();
    for line in file.lines() {
        let l = line.unwrap();
        if l.starts_with("#") || l.len() == 0 {
            continue;
        }
        let sp: Vec<&str> = l.split(":").collect();
        if sp.len() >= 2 {
            let t = BTransform {
                book_name: None,
                chapter: None,
                verse: None,
                original: sp[0].to_owned(),
                replacement: sp[1].to_owned(),
            };
            subs.push(t);
        }
    }
    subs
}

fn get_verses(db: Connection, book: &str, chapter_range: &str) -> Vec<BVerse> {
    let stmt = format!("SELECT cast(BookID as int) as BookID,
        BookAbr, BookName,
        cast(Chapter as int) as Chapter,
        cast(Verse as int) as Verse,
        VText FROM Bible WHERE BookName like '{}%' AND Chapter in ({}) order by cast(chapter as int), cast(verse as int);", book, chapter_range);
    let mut prepared_stmt = db.prepare(stmt.as_str()).unwrap();

    let vs = prepared_stmt
        .query_map(NO_PARAMS, |row| {
            Ok(BVerse {
                book_no: row.get(0).unwrap(),
                book_abr: row.get(1).unwrap(),
                book_name: row.get(2).unwrap(),
                chapter: row.get(3).unwrap(),
                verse: row.get(4).unwrap(),
                verse_text: row.get(5).unwrap(),
            })
        })
        .unwrap();

    let mut verses: Vec<BVerse> = Vec::new();
    for v in vs {
        verses.push(v.unwrap())
    }

    verses
}

fn get_book(db: &Connection, book_id: i32) -> Vec<BVerse> {
    let stmt = format!(
        "SELECT cast(BookID as int) as BookID,
        BookAbr, BookName,
        cast(Chapter as int) as Chapter,
        cast(Verse as int) as Verse,
        VText FROM Bible WHERE BookID = '{}' order by cast(chapter as int), cast(verse as int);",
        book_id
    );
    let mut prepared_stmt = db.prepare(stmt.as_str()).unwrap();

    let vs = prepared_stmt
        .query_map(NO_PARAMS, |row| {
            Ok(BVerse {
                book_no: row.get(0).unwrap(),
                book_abr: row.get(1).unwrap(),
                book_name: row.get(2).unwrap(),
                chapter: row.get(3).unwrap(),
                verse: row.get(4).unwrap(),
                verse_text: row.get(5).unwrap(),
            })
        })
        .unwrap();

    let mut verses: Vec<BVerse> = Vec::new();
    for v in vs {
        verses.push(v.unwrap())
    }

    verses
}

fn update_book(conn: &Connection, verses: &Vec<BVerse>) {
    let stmt = "UPDATE BIBLE SET TText=?1 where BookID=?2 and Chapter=?3 and Verse=?4";
    let mut prepared_stmt = conn.prepare(stmt).unwrap();

    for verse in verses {
        let _ = prepared_stmt.execute(&[
            &verse.verse_text,
            &verse.book_no.to_string(),
            &verse.chapter.to_string(),
            &verse.verse.to_string(),
        ]);
    }
}

fn generate_html(new_verses: Vec<BVerse>) -> String {
    let mut s = String::new();
    let mut current_para = String::new();
    let mut current_verse: String;
    let mut book = String::new();
    let mut chapter: i64 = 0;
    for v in new_verses {
        if book != v.book_name
            || chapter != v.chapter
            || v.verse_text.contains("¶ ")
            || v.book_name == "Psalms"
        {
            s.push_str(
                format!(
                    "{}",
                    html! {
                        p {
                            :Raw(format!("{}", current_para));
                        }
                    }
                )
                .as_str(),
            );
            current_para.clear();
        }
        if book != v.book_name {
            s.push_str(
                format!(
                    "{}",
                    html! {
                        h1{
                            : format_args!("{}", v.book_name);
                        }
                    }
                )
                .as_str(),
            );
            book = v.book_name.clone();
        }
        if chapter != v.chapter {
            s.push_str(
                format!(
                    "{}",
                    html! {
                        h2: format_args!("Chapter {}", v.chapter);
                    }
                )
                .as_str(),
            );
            chapter = v.chapter.clone();
        }
        if v.verse_text.contains("¶ ") {
            current_verse = v.verse_text.replace("¶ ", "");
            current_para.push_str(
                format!(
                    "{}",
                    html! {
                        span {
                            span {
                                : format_args!("({}:{})", v.chapter, v.verse)
                            }
                            : format_args!(" {}", current_verse)
                        }
                        br {
                        }
                    }
                )
                .as_str(),
            )
        } else {
            current_verse = v.verse_text.clone();
            current_para.push_str(
                format!(
                    "{}",
                    html! {
                        span {
                            span {
                                : format_args!(" ({}:{})", v.chapter, v.verse)
                            }
                            : format_args!(" {}", current_verse)
                        }
                        br {
                        }
                    }
                )
                .as_str(),
            )
        }
    }
    s.push_str(
        format!(
            "{}",
            html! {
                p {
                    :Raw(format!("{}", current_para));
                }
            }
        )
        .as_str(),
    );
    current_para.clear();
    format!(
        "{}",
        html! {
            html {
                head{}
                body{
                    :Raw(format!("{}", s))
                }
            }
        }
    )
}

fn old_main() {
    let subs = parse_transforms("../bible_transforms.txt");

    let conn = Connection::open("../KJV-PCE.db").unwrap();

    let args: Vec<String> = std::env::args().collect();

    let verses = get_verses(conn, args[1].as_str(), args[2].as_str());

    let mut new_verses: Vec<BVerse> = Vec::new();
    for verse in verses {
        new_verses.push(apply_substitutions_verse(&subs, &verse));
    }

    let html = generate_html(new_verses);
    print!("{}", html)
    //for line in io::stdin().lock().lines() {
    //    apply_substitutions(line.unwrap(), &subs)
    //}
}

fn main() {
    let subs = parse_transforms("../bible_transforms.txt");
    let conn = Connection::open("../KJV-PCE.db").unwrap();

    for i in 1..67 {
        println!("Updating book {}", i);
        let verses = get_book(&conn, i as i32);
        let mut new_verses: Vec<BVerse> = Vec::new();
        for verse in &verses {
            new_verses.push(apply_substitutions_verse(&subs, verse));
        }
        update_book(&conn, &new_verses)
    }
}
