package main

import (
	"bytes"
	"fmt"
	"html"
	"log"
	"os"
	"strings"

	"github.com/cbroglie/mustache"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	dmp "github.com/sergi/go-diff/diffmatchpatch"
)

// Verse is a single Bible verse
type Verse struct {
	BookID   string
	BookAbr  string
	BookName string
	Chapter  string
	Verse    string
	VText    string
	TText    string
}

// struct of data passed to the mustache template
type writeData struct {
	BookName string
	BookAbr  string
	Chapter  string
	PrevChap string
	NextChap string
	Verses   []Verse
}

func getBooks(db *sqlx.DB) []string {
	rows, err := db.Query("select distinct BookID from Bible;")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	res := []string{}

	for rows.Next() {
		var id string
		err = rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, id)
	}

	return res
}

func getVerses(db *sqlx.DB, bookID string) []Verse {
	rows, err := db.Queryx("select * from Bible where BookID=?", bookID)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	res := []Verse{}
	for rows.Next() {
		var verse Verse
		err = rows.StructScan(&verse)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, verse)
	}
	log.Printf("%d verses in book %s", len(res), bookID)
	return res
}

func writePages(tmpl *mustache.Template, verses []Verse) error {
	chapter := "1"
	prevChap := ""
	vs := []Verse{}
	for _, verse := range verses {
		if verse.VText != verse.TText {
			d := dmp.New()
			diffs := d.DiffMain(verse.VText, verse.TText, false)
			verse.TText = diffPrettyHTML(diffs)
		}

		if verse.Chapter != chapter {
			data := writeData{
				BookName: vs[0].BookName,
				BookAbr:  vs[0].BookAbr,
				Chapter:  vs[0].Chapter,
				PrevChap: prevChap,
				NextChap: verse.Chapter,
				Verses:   vs,
			}
			prevChap = chapter
			writePage(tmpl, &data)
			vs = []Verse{}
		}
		vs = append(vs, verse)
		chapter = verse.Chapter
	}
	data := writeData{
		BookName: vs[0].BookName,
		BookAbr:  vs[0].BookAbr,
		Chapter:  vs[0].Chapter,
		PrevChap: prevChap,
		NextChap: "",
		Verses:   vs,
	}
	writePage(tmpl, &data)
	return nil
}

func writePage(tmpl *mustache.Template, data *writeData) {
	f, _ := os.Create(fmt.Sprintf("output/%s_%s.html", data.BookAbr, data.Chapter))
	log.Printf("Writing %s %s", data.BookName, data.Chapter)
	tmpl.FRender(f, &data)
	f.Close()
}

func diffPrettyHTML(diffs []dmp.Diff) string {
	var buff bytes.Buffer
	for _, diff := range diffs {
		text := strings.Replace(html.EscapeString(diff.Text), "\n", "&para;<br>", -1)
		switch diff.Type {
		case dmp.DiffInsert:
			_, _ = buff.WriteString("<span class=diffadd>")
			_, _ = buff.WriteString(text)
			_, _ = buff.WriteString("</span>")
		case dmp.DiffDelete:
			_, _ = buff.WriteString("<span class=diffrem>")
			_, _ = buff.WriteString(text)
			_, _ = buff.WriteString("</span>")
		case dmp.DiffEqual:
			_, _ = buff.WriteString(text)
		}
	}
	return buff.String()
}

//func diffPrettyHTML2(diffs []dmp.Diff) string {
//	var buff string
//	var sl []string
//	var lastWord string
//	lastOp := dmp.DiffEqual
//	for _, diff := range diffs {
//		text := strings.Replace(html.EscapeString(diff.Text), "\n", "&para;<br>", -1)
//		switch diff.Type {
//		case dmp.DiffInsert:
//			buff = strings.TrimSuffix(buff, lastWord)
//			buff += "<span class=diffadd>"
//			buff += lastWord
//			buff += text
//			buff += "</span>"
//		case dmp.DiffDelete:
//			buff = strings.TrimSuffix(buff, lastWord)
//			buff += "<span class=diffrem>"
//			buff += lastWord
//			buff += text
//			buff += "</span>"
//		case dmp.DiffEqual:
//			if lastOp != dmp.DiffEqual {
//			}
//			buff += text
//			if strings.HasSuffix(buff, " ") {
//				sl := strings.Split(buff, " ")
//				lastWord = sl[len(sl)-1]
//			}
//		}
//
//	}
//	return buff
//}

func main() {

	db, err := sqlx.Open("sqlite3", "../KJV-PCE.db")
	db.MapperFunc(func(x string) string { return x })
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	bookTmpl, err := mustache.ParseFile("book.mustache")
	if err != nil {
		log.Fatal(err)
	}

	books := getBooks(db)
	for _, book := range books {
		log.Printf("Processing book %s", book)
		verses := getVerses(db, book)
		writePages(bookTmpl, verses)
	}
}
